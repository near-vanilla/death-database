package near.vanilla.death.database

import com.google.gson.*
import io.leangen.geantyref.TypeToken
import org.bukkit.Bukkit
import org.bukkit.configuration.serialization.ConfigurationSerializable
import org.bukkit.configuration.serialization.ConfigurationSerialization
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.ItemMeta
import org.bukkit.util.io.BukkitObjectInputStream
import org.bukkit.util.io.BukkitObjectOutputStream
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.lang.Exception
import java.lang.IllegalStateException

// Simple wrappers for easier on eyes coding
fun warn(message : String, throwable : Throwable) =
        Bukkit.getLogger().warning("${PluginLifecycle.TAG}: $message - ${throwable.message}")

fun warn(message : String) =
        Bukkit.getLogger().warning("${PluginLifecycle.TAG}: $message")

fun info(message : String) =
        Bukkit.getLogger().info("${PluginLifecycle.TAG}: $message")

val STRING_TO_ANY = object : TypeToken<Map<String, Any>>(){}.type!!

fun toJSON(items : List<ItemStack>) : String {
    val jsonArray = JsonArray()
    items.forEach {
        // Null check needed here probably because of kotlin -> java interop
        if(it != null){
            val itemStackMap = it.serialize()
            val itemStackJSON = Gson().toJson(itemStackMap, STRING_TO_ANY)
            val itemStackJSONObject = JsonParser().parse(itemStackJSON).asJsonObject
            if(it.hasItemMeta()){
                // Have to serialize meta separately
                itemStackJSONObject.addProperty("meta", toBase64(it.itemMeta!!))
            }
            jsonArray.add(itemStackJSONObject)
        }
    }
    return jsonArray.toString()
}

fun fromJSON(itemString : String) : List<ItemStack> {
    val parser = JsonParser()
    val element = parser.parse(itemString)
    if(!element.isJsonArray){
        return emptyList()
    }
    val jsonArray = element.asJsonArray
    return jsonArray.map {
        val itemStackMapRep = Gson().fromJson<Map<String, Any>>(it, STRING_TO_ANY).toMutableMap()
        val itemStack = ItemStack.deserialize(itemStackMapRep)
        if(itemStackMapRep.containsKey("meta")){
            try{
                itemStack.itemMeta = fromBase64(itemStackMapRep["meta"].toString())
            }catch (ex : Exception){
                warn("Couldn't deserialize meta", ex)
            }
        }
        itemStack
    }
}

fun toBase64(meta : ItemMeta) : String {
    val outputStream = ByteArrayOutputStream()
    val bukkitOutputStream = BukkitObjectOutputStream(outputStream)
    bukkitOutputStream.writeObject(meta)
    bukkitOutputStream.close()
    return Base64Coder.encodeLines(outputStream.toByteArray())
}

fun fromBase64(itemMetaString : String) : ItemMeta? {
    val inputStream = ByteArrayInputStream(Base64Coder.decodeLines(itemMetaString))
    val bukkitInputStream = BukkitObjectInputStream(inputStream)
    val obj = bukkitInputStream.readObject()
    if(obj is ItemMeta){
        return obj
    }
    return null
}
